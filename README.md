# Image quality improvement based on a generative adversarial network

A source code for the Tallinn University of Technology IT College final thesis project.

### Author
- Artyom Likhachev

## Problem statement
The goal of this thesis would be to build a program based on machine learning technology and specifically using a generative adversarial network approach, for the digital camera photo enhancement. The program should take an image as input, upscale it and noticeably improve its quality. The developed solution would be an experiment to evaluate such approach suitability for handling real-world data. As the level of noise and the optical degradation pattern could be unique for each digital camera and even optical lens, the scope of this project is narrowed from a general-purpose solution to a program that works specifically with the camera model that was used for dataset creation. Due to the solution experimental nature, and keeping in mind that the level of improved quality is a very subjective notion, the author does not set a strict goal for the program accuracy.

## GAN Architecture
![GAN Architecture](./misc/description_images/architecture.png)

## Outputs
Examples of photos from the dataset that were processed by the program. The left sample is taken from a low-resolution photo. The middle sample shows the program effect. The right sample is the ground truth (GT) from the real high-resolution image.
A few examples of a full-size photos couild be found in the `misc.example_photos` folder.

![Example1](./misc/description_images/example1.png)
![Example2](./misc/description_images/example2.png)
![Example3](./misc/description_images/example3.png)
![Example4](./misc/description_images/example4.png)
![Example5](./misc/description_images/example5.png)
![Example6](./misc/description_images/example6.png)
![Example7](./misc/description_images/example7.png)

## Project structure
The source code includes the program to enhance the photo in a form of Python file. It comes with a command-line interface for quick parameters change. Additionally to that, the repository includes all other code parts used in this work: tools for dataset automatic creation, utils that were used for work visualization, a thesis GAN model, its trained weights and code for the training.
A files marked as "Executable" could be ececuted from the console with the additional parameters.

#### **`enhance_photo.py`** 
This is the main program file to enhance the photo. It iterates through the entire input folder and enhances all images in it. Executable.  
`--input_folder` -   A folder with input images to enhance.  
`--weights_path` -   A path to the generator weights.  
`--model_shaper` -   A Generator model shape, than that would be used as input to the generator. Default:`64`  
`--target_shape` -   A target shape size. Default:`128`  
`--batch_size` -   A batch size would be used during the generator prediction. Default:`24`  

#### `dataset_generator`
* **`create_dataset.py`** - Creates a dataset from camera photo folder in a fully automated way. Executable.  
`--image_format` -   An image format for the dataset. Default:`'.JPG'`  
`--focal_length_lr` -   A focal length for low-resolution samples. Default:`16`  
`--focal_length_hr` -   A focal length for high-resolution samples. Default:`50`  
`--path_input` -   A path with images folder to create the dataset from.  
`--path_output` -   An output location for saving the dataset.   
The script expects from input folder to be sorted in way, that high-resolution photo (long focal length) comes first, and then followed by the same scene, but in a lower resolution (short focal length). For instance: Photo_1_50mm.jpg, Photo_1_30mm.jpg, Photo_1_16mm.jpg, Photo_2_50mm.jpg, Photo_2_30mm.jpg, Photo_2_16mm.jpg, etc.  

* **`match_single_image_pair.py`** - Matches two images and shows visualization of it. Executable. Was used for thesis and testing purposes.  
`--hr_img_path` -   A path to high-resolution image.  
`--lr_img_path` -   A path to low-resolution image.  
`--path_to_save` -   A path to save the examples.  
`--fast_matching` -   A flag to enable fast image matching.  

* **`image_processor.py`** - The image processor for a precise image pairs matching and cutting.  

#### `gan`
* **`batch_generator.py`** - A helper class to generate training data batches.  
* **`model.py`** - A project's neural network model: Generator, Discriminator, GAN. Executable - shows the network architecture.  

* **`train.py`** - Performs a model training on the provided dataset. Saves weights and logs. Executable.  
`--data_format` -   The image data format. Default: `channels_last`  
`--batch_size` -   A training batch size. Default: `24`  
`--dataset_shuffle` -   A flag to shufle the dataset. Default: `1`  
`--start_iteration` -   A training starting iteration, if contunues from previous training. Default: `0`  
`--num_iterations` -   A number of training iterations. Default: `1000000`  
`--model_checkpoint_freq_weights` -   A frequency of iterations to save the model weigts.  Default: `1000`  
`--model_checkpoint_freq_loss` -   A frequency of iterations to save the model loss. Default: `50`  
`--path_hr` -   A path to high-resolution samples.  
`--path_lr` -   A path to low-resolution samples.  
`--path_weights` -   A location to save the weights.   
`--path_discriminator_weights` -   A path to Discriminator model weights. Optional. Needed only if training continues previous session.  
`--path_generator_weights` -   A path to Generator model weights. Optional. Needed only if training continues previous session.  
`--path_logs_excel` -   A location to save excel file with loss logs.  
`--training_name` -   A name of the current training.  

* **`utils.py`** - Contains helper util methods.  

#### `thesis_examples`
* **`examples_comparison_tool.py`** - A tool used for plotting the output examples needed for thesis visualization.  
* **`losses_plot_util.py`** - A tool used for model losses visualization needed in thesis document.  

#### `weights`
* **`discriminator_16_50_batch24_final.py`** - A pretrained discriminator model weights.  
* **`generator_16_50_batch24_final.py`** - A pretrained generator model weights. Could be used for image enhancement.  


## Environment requirements
- For the correct program work it requires installation of Python 3.6.X.
- Additional libraries: TensorFlow (tensorflow-gpu, or just tensorflow for running on cpu), OpenCV, NumPy, Pillow, Click, Matplotlib, tqdm, xlrd, xlwt
- Anaconda enviromnent snapshot could be found in `setup` folder. Or you can use `setup.requrements.txt` for the manual installation.

#### Setup from Anaconda snapshot

Create a new [conda](https://conda.io) environment from yaml file and then activate it.

    conda env create -f environment.yml
    conda activate Thesis-MySRGAN


## References
* **(1)** Ledig, C.; Theis, L.; Huszar, F.; Caballero, J.; Aitken, A. P.; Tejani, A.; Totz, J.; Wang, Z., Shi, W., "Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network," CoRR, 2016.
https://arxiv.org/pdf/1609.04802.pdf
* **(2)** https://stackoverflow.com/questions/47862262/how-to-subtract-channel-wise-mean-in-keras/47862836#47862836
* **(3)** https://datascience.stackexchange.com/questions/56377/how-and-why-to-rescale-image-range-between-0-1-and-1-1
* **(4)** https://github.com/keras-team/keras-preprocessing/blob/master/keras_preprocessing/image/iterator.py
* **(5)** Introduction to SURF (Speeded-Up Robust Features)
    https://docs.opencv.org/trunk/df/dd2/tutorial_py_surf_intro.html
* **(6)** Feature Matching + Homography to find Objects
    https://docs.opencv.org/trunk/d1/de0/tutorial_py_feature_homography.html 
     